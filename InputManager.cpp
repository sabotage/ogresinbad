#include "InputManager.h"
#include <SDL_syswm.h>

#include <OgrePlatform.h>
#include <OgreRoot.h>



/// \brief General purpose wrapper for OGRE applications around SDL's event
///        queue, mostly used for handling input-related events.
//InputManager::InputManager(SDL_Window* window, Ogre::RenderWindow* ogreWindow, bool grab) :
InputManager::InputManager(bool grab) :
    ////mSDLWindow(window),
    ////mOgreWindow(ogreWindow),
    mWarpCompensate(false),
    mMouseRelative(false),
    mGrabPointer(false),
    mWrapPointer(false),
    mMouseZ(0),
    mMouseY(0),
    mMouseX(0),
    mMouseInWindow(true),
    mJoyListener(NULL),
    mKeyboardListener(NULL),
    mMouseListener(NULL),
    mWindowListener(NULL),
    mWindowHasFocus(true),
    mWantGrab(false),
    mWantRelative(false),
    mWantMouseVisible(false),
    mAllowGrab(grab)
{
    //_setupOISKeys();
}

InputManager::~InputManager()
{
}

void InputManager::capture(bool windowEventsOnly)
{
    SDL_PumpEvents();

    SDL_Event evt;
    
    
    //if (windowEventsOnly)
    //{
        // During loading, just handle window events, and keep others for later
   //     while (SDL_PeepEvents(&evt, 1, SDL_GETEVENT, SDL_WINDOWEVENT, SDL_WINDOWEVENT))
    //        handleWindowEvent(evt);
    //    return;
   //// }
    
    //std::cout << "RUNNING_______________RUNNING" << std::endl;

    while(SDL_PollEvent(&evt))
    {
        std::cout << evt.type <<std::endl;

        switch(evt.type)
        {
                
            case SDL_MOUSEMOTION:
                // Ignore this if it happened due to a warp
               // if(!_handleWarpMotion(evt.motion))
                //{
                    // If in relative mode, don't trigger events unless window has focus
                  //  if (!mWantRelative || mWindowHasFocus)
                        mMouseListener->mouseMoved(_packageMouseMotion(evt));
                
                std::cout << _packageMouseMotion(evt).x << std::endl;
                    // Try to keep the mouse inside the window
                    if (mWindowHasFocus)
                        _wrapMousePointer(evt.motion);
                //}
                break;
            case SDL_MOUSEWHEEL:
                mMouseListener->mouseMoved(_packageMouseMotion(evt));
                break;
            case SDL_MOUSEBUTTONDOWN:
                
                mMouseListener->mousePressed(evt.button, evt.button.button);
                break;
            case SDL_MOUSEBUTTONUP:
                mMouseListener->mouseReleased(evt.button, evt.button.button);
                break;
            case SDL_KEYDOWN:
                if (!evt.key.repeat)
                    mKeyboardListener->keyPressed(evt.key);
                break;
            case SDL_KEYUP:
                if (!evt.key.repeat)
                    mKeyboardListener->keyReleased(evt.key);
                break;
            case SDL_TEXTINPUT:
                mKeyboardListener->textInput(evt.text);
                break;
            case SDL_JOYAXISMOTION:
                if (mJoyListener)
                    mJoyListener->axisMoved(evt.jaxis, evt.jaxis.axis);
                break;
            case SDL_JOYBUTTONDOWN:
                if (mJoyListener)
                    mJoyListener->buttonPressed(evt.jbutton, evt.jbutton.button);
                break;
            case SDL_JOYBUTTONUP:
                if (mJoyListener)
                    mJoyListener->buttonReleased(evt.jbutton, evt.jbutton.button);
                break;
            case SDL_JOYDEVICEADDED:
                //SDL_JoystickOpen(evt.jdevice.which);
                //std::cout << "Detected a new joystick: " << SDL_JoystickNameForIndex(evt.jdevice.which) << std::endl;
                break;
            case SDL_JOYDEVICEREMOVED:
                //std::cout << "A joystick has been removed" << std::endl;
                break;
            case SDL_WINDOWEVENT:
                handleWindowEvent(evt);
                break;
            case SDL_QUIT:
                if (mWindowListener)
                    mWindowListener->windowClosed();
                break;
            default:
                std::cerr << "Unhandled SDL event of type " << evt.type << std::endl;
                break;
        }
    }
}

void InputManager::handleWindowEvent(const SDL_Event& evt)
{
    switch (evt.window.event) {
        case SDL_WINDOWEVENT_ENTER:
            mMouseInWindow = true;
            updateMouseSettings();
            break;
        case SDL_WINDOWEVENT_LEAVE:
            mMouseInWindow = false;
            updateMouseSettings();
            break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            int w,h;
            //SDL_GetWindowSize(//mSDLWindow, &w, &h);
            // TODO: Fix Ogre to handle this more consistently (fixed in 1.9)
#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
            //mOgreWindow->resize(w, h);
#else
           // //mOgreWindow->windowMovedOrResized();
#endif
            if (mWindowListener)
                mWindowListener->windowResized(w, h);
            break;

        case SDL_WINDOWEVENT_RESIZED:
            // TODO: Fix Ogre to handle this more consistently (fixed in 1.9)
#if OGRE_PLATFORM == OGRE_PLATFORM_LINUX
            //mOgreWindow->resize(evt.window.data1, evt.window.data2);
#else
            //mOgreWindow->windowMovedOrResized();
#endif
            if (mWindowListener)
                mWindowListener->windowResized(evt.window.data1, evt.window.data2);
            break;

        case SDL_WINDOWEVENT_FOCUS_GAINED:
            mWindowHasFocus = true;
            updateMouseSettings();
            if (mWindowListener)
                mWindowListener->windowFocusChange(true);

            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            mWindowHasFocus = false;
            updateMouseSettings();
            if (mWindowListener)
                mWindowListener->windowFocusChange(false);
            break;
        case SDL_WINDOWEVENT_CLOSE:
            break;
        case SDL_WINDOWEVENT_SHOWN:
            //mOgreWindow->setVisible(true);
            if (mWindowListener)
                mWindowListener->windowVisibilityChange(true);
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            //mOgreWindow->setVisible(false);
            if (mWindowListener)
                mWindowListener->windowVisibilityChange(false);
            break;
    }
}

bool InputManager::isModifierHeld(SDL_Keymod mod)
{
    return SDL_GetModState() & mod;
}

bool InputManager::isKeyDown(SDL_Scancode key)
{
    return SDL_GetKeyboardState(NULL)[key];
}

/// \brief Moves the mouse to the specified point within the viewport
void InputManager::warpMouse(int x, int y)
{
    //SDL_WarpMouseInWindow(//mSDLWindow, x, y);
    mWarpCompensate = true;
    mWarpX = x;
    mWarpY = y;
}

/// \brief Locks the pointer to the window
void InputManager::setGrabPointer(bool grab)
{
    mWantGrab = grab;
    updateMouseSettings();
}

/// \brief Set the mouse to relative positioning. Doesn't move the cursor
///        and disables mouse acceleration.
void InputManager::setMouseRelative(bool relative)
{
    mWantRelative = relative;
    updateMouseSettings();
}

void InputManager::setMouseVisible(bool visible)
{
    mWantMouseVisible = visible;
    updateMouseSettings();
}

void InputManager::updateMouseSettings()
{
    mGrabPointer = mWantGrab && mMouseInWindow && mWindowHasFocus;
    //SDL_SetWindowGrab(//mSDLWindow, mGrabPointer && mAllowGrab ? SDL_TRUE : SDL_FALSE);

    SDL_ShowCursor(mWantMouseVisible || !mWindowHasFocus);

    bool relative = mWantRelative && mMouseInWindow && mWindowHasFocus;
    if(mMouseRelative == relative)
        return;

    mMouseRelative = relative;

    mWrapPointer = false;

    //eep, wrap the pointer manually if the input driver doesn't support
    //relative positioning natively
    int success = SDL_SetRelativeMouseMode(relative ? SDL_TRUE : SDL_FALSE);
    if(relative && success != 0)
        mWrapPointer = true;

    //now remove all mouse events using the old setting from the queue
    SDL_PumpEvents();
    SDL_FlushEvent(SDL_MOUSEMOTION);
}

/// \brief Internal method for ignoring relative motions as a side effect
///        of warpMouse()
bool InputManager::_handleWarpMotion(const SDL_MouseMotionEvent& evt)
{
    if(!mWarpCompensate)
        return false;

    //this was a warp event, signal the caller to eat it.
    if(evt.x == mWarpX && evt.y == mWarpY)
    {
        mWarpCompensate = false;
        return true;
    }

    return false;
}

/// \brief Wrap the mouse to the viewport
void InputManager::_wrapMousePointer(const SDL_MouseMotionEvent& evt)
{
    //don't wrap if we don't want relative movements, support relative
    //movements natively, or aren't grabbing anyways
    if(!mMouseRelative || !mWrapPointer || !mGrabPointer)
        return;

    int width = 0;
    int height = 0;

    //SDL_GetWindowSize(//mSDLWindow, &width, &height);

    const int FUDGE_FACTOR_X = width;
    const int FUDGE_FACTOR_Y = height;

    //warp the mouse if it's about to go outside the window
    if(evt.x - FUDGE_FACTOR_X < 0  || evt.x + FUDGE_FACTOR_X > width
            || evt.y - FUDGE_FACTOR_Y < 0 || evt.y + FUDGE_FACTOR_Y > height)
    {
        warpMouse(width / 2, height / 2);
    }
}

/// \brief Package mouse and mousewheel motions into a single event
MouseMotionEvent InputManager::_packageMouseMotion(const SDL_Event &evt)
{
    MouseMotionEvent pack_evt;
    pack_evt.x = mMouseX;
    pack_evt.xrel = 0;
    pack_evt.y = mMouseY;
    pack_evt.yrel = 0;
    pack_evt.z = mMouseZ;
    pack_evt.zrel = 0;

    if(evt.type == SDL_MOUSEMOTION)
    {
        pack_evt.x = mMouseX = evt.motion.x;
        pack_evt.y = mMouseY = evt.motion.y;
        pack_evt.xrel = evt.motion.xrel;
        pack_evt.yrel = evt.motion.yrel;
    }
    else if(evt.type == SDL_MOUSEWHEEL)
    {
        mMouseZ += pack_evt.zrel = (evt.wheel.y * 120);
        pack_evt.z = mMouseZ;
    }
    else
    {
        throw new std::runtime_error("Tried to package non-motion event!");
    }

    return pack_evt;
}

  



