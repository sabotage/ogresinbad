#ifndef _INPUTMANAGER_H
#define _INPUTMANAGER_H

#include <SDL_events.h>

#include <OgreRenderWindow.h>
//#include <boost/unordered_map.hpp>
//#include "OISCompat.h"
#include "input.h"


class InputManager
{
public:
    //InputManager(SDL_Window *window, Ogre::RenderWindow* ogreWindow, bool grab);
    InputManager(bool grab);
    ~InputManager();

    void setMouseEventCallback(MouseListener* listen) { mMouseListener = listen; }
    void setKeyboardEventCallback(KeyListener* listen) { mKeyboardListener = listen; }
    void setWindowEventCallback(WindowListener* listen) { mWindowListener = listen; }
	void setJoyEventCallback(JoyListener* listen) { mJoyListener = listen; }

    void capture(bool windowEventsOnly);
	bool isModifierHeld(SDL_Keymod mod);
	bool isKeyDown(SDL_Scancode key);

    void setMouseVisible (bool visible);
    void setMouseRelative(bool relative);
    bool getMouseRelative() { return mMouseRelative; }
    void setGrabPointer(bool grab);

    void warpMouse(int x, int y);

    void updateMouseSettings();

private:

    void handleWindowEvent(const SDL_Event& evt);

    bool _handleWarpMotion(const SDL_MouseMotionEvent& evt);
    void _wrapMousePointer(const SDL_MouseMotionEvent &evt);
    MouseMotionEvent _packageMouseMotion(const SDL_Event& evt);



    MouseListener* mMouseListener;
    KeyListener* mKeyboardListener;
    WindowListener* mWindowListener;
	JoyListener* mJoyListener;

    Uint16 mWarpX;
    Uint16 mWarpY;
    bool mWarpCompensate;
    bool mWrapPointer;

    bool mAllowGrab;
    bool mWantMouseVisible;
    bool mWantGrab;
    bool mWantRelative;
    bool mGrabPointer;
    bool mMouseRelative;

    Sint32 mMouseZ;
    Sint32 mMouseX;
    Sint32 mMouseY;

    bool mWindowHasFocus;
    bool mMouseInWindow;

    //SDL_Window* mSDLWindow;
    //Ogre::RenderWindow* mOgreWindow;
};



#endif
