#include <Ogre.h>
#include "SDL.h"
#include "SDL_syswm.h"
#include "OgreDemoApp.h"

int main(int argc, char *argv[])
{
    
    
    DemoApp demo;

    demo.startDemo();
    
    
    bool done = false;
    
    SDL_Event event;
    
    double mStartTime;
    double mLastFrameTime;

        while (!done)
        {
            
            
            mStartTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU();
            
			OgreFramework::getSingletonPtr()->updateOgre(mLastFrameTime);
			OgreFramework::getSingletonPtr()->m_pRoot->renderOneFrame();
            
			mLastFrameTime = OgreFramework::getSingletonPtr()->m_pTimer->getMillisecondsCPU() - mStartTime;
            
            
            SDL_PumpEvents();

            while (SDL_PollEvent(&event))
            {
                std::cout << event.type << std::endl;
                switch (event.type)
                {
                    case SDL_QUIT:
                        done = true;
                        break;
                        
                    case SDL_APP_DIDENTERFOREGROUND:
                        SDL_Log("SDL_APP_DIDENTERFOREGROUND");
                        break;
                        
                    case SDL_APP_DIDENTERBACKGROUND:
                        SDL_Log("SDL_APP_DIDENTERBACKGROUND");
                        break;
                        
                    case SDL_APP_LOWMEMORY:
                        SDL_Log("SDL_APP_LOWMEMORY");
                        break;
                        
                    case SDL_APP_TERMINATING:
                        SDL_Log("SDL_APP_TERMINATING");
                        break;
                        
                    case SDL_APP_WILLENTERBACKGROUND:
                        SDL_Log("SDL_APP_WILLENTERBACKGROUND");
                        break;
                        
                    case SDL_APP_WILLENTERFOREGROUND:
                        SDL_Log("SDL_APP_WILLENTERFOREGROUND");
                        break;
                        
                    case SDL_WINDOWEVENT:
                    {
                        switch (event.window.event)
                        {
                        
                            case SDL_WINDOWEVENT_RESIZED:
                            {
                                SDL_Log("Window %d resized to %dx%d", event.window.windowID, event.window.data1, event.window.data2);
                                
                                break;
                            }
                        }
                    }
                }
            }
        //}
        
    }
    
    //SDL_GL_DeleteContext(gl);
    //SDL_DestroyWindow(window);
    //SDL_Quit();

    
  
    return 0;
}

